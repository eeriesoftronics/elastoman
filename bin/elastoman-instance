#!/usr/bin/env perl

use 5.010;
use strict;
use warnings;

BEGIN {
	use Cwd qw/abs_path/;
	use File::Basename;
	if (-d dirname(abs_path($0)) . '/../lib') {
		unshift @INC, dirname(abs_path($0)) . '/../lib';
	}
	if (-d dirname(abs_path($0)) . '/../extlib') {
		unshift @INC, dirname(abs_path($0)) . '/../extlib';
	}
}

use Archive::Tar;
use Cwd              qw/abs_path getcwd/;
use Data::Dumper;
use File::Basename;
use File::Copy       qw/cp/;
use File::Path       qw/mkpath/;
use Getopt::Long;
use Pod::Usage;
use Term::ANSIColor ':constants';

use YAML::Tiny;
#use LWP::Simple;

use Elastoman::Instance;
use Elastoman::Server;

sub esm_error($) {
	my $err_msg = shift;
	
	pod2usage(
		-message => RED . $err_msg . RESET,
		-exitval => 1,
	);
}

my $opts = {
	'config-file' => dirname($0) . '/instances.yml',
};

GetOptions($opts, 'help', 'config-file=s');

pod2usage(-exitval => 0, -verbose => 1) if $opts->{help};

die "No configuration file" unless -f $opts->{'config-file'};
my $cfg = YAML::Tiny->read($opts->{'config-file'}) or die 'could not parse config file: ' . YAML::Tiny->errstr;
$cfg = $cfg->[0];

my $servers = {};
foreach (keys %{$cfg->{'servers'}}) {
	$servers->{$_} = Elastoman::Server->new($_, $cfg->{'servers'}->{$_});
}

my $instances = {};
foreach (keys %{$cfg->{'instances'}}) {
	my $icfg = $cfg->{'instances'}->{$_};
	die 'invalid server in ', $_, ' instance configuration' unless defined $servers->{$icfg->{'server'}};
	$icfg->{'server'} = $servers->{$icfg->{'server'}};
	$instances->{$_} = Elastoman::Instance->new($_, $icfg);
}

# read the action
my $action = shift || esm_error('You need to provide an action');

# main 'loop'

if ($action eq 'list') {
	
	# list all instances
	print YELLOW, 'Instances: ', "\n";
	foreach (keys %$instances) {
		print "\t", YELLOW, sprintf("%15s", $_), ': ';
		my $_s = $instances->{$_}->status;
		print GREEN, 'n/a ' if $_s == -1;
		print GREEN, 'down' if $_s ==  0;
		print GREEN, 'up  ' if $_s ==  1;
		if ($instances->{$_}->server->installed) {
			print RESET, '[server installed]';
		} else {
			print RESET, '[no server installed]';
		}
		print "\n";
	}
	
} elsif ($action eq 'setup') {
	
	# deploy a new instance
	my $instance = shift;

	die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$instance};
	unless ($instances->{$instance}->server->installed) {
		print YELLOW, 'Server is not installed, installing...', RESET, "\n";
		$instances->{$instance}->server->install or die 'failed to install server';
		print GREEN, 'Server is installed', RESET, "\n";
	}
	$instances->{$instance}->deploy or die 'failed to setup the instance';
	print GREEN, 'Instance setup complete.', RESET, "\n";

} elsif ($action eq 'start') {
	
	# start a new instance
	my $instance = shift;
	die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$instance};
	unless ($instances->{$instance}->server->installed) {
		print YELLOW, 'Server is not installed, installing...', RESET, "\n";
		$instances->{$instance}->server->install or die 'failed to install server';
		print GREEN, 'Server is installed', RESET, "\n";
	}
	unless ($instances->{$instance}->installed == 1) {
		print YELLOW, 'Instance is not configured, configuring...', RESET, "\n";
		$instances->{$instance}->deploy or die 'failed to configure instance';
		print GREEN, 'Instance is configured.', RESET, "\n";
	}
	$instances->{$instance}->start;
	
	sleep 1;
	if ($instances->{$instance}->status > 0) {
		print GREEN, 'Started: ', RESET, $instance, "\n";
	} else {
		print RED, 'Failed to start: ', RESET, $instance, "\n";
	}
	
} elsif ($action eq 'stop') {
	
	# stop an instance
	my $instance = shift;
	die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$instance};
	$instances->{$instance}->stop;

	print GREEN, 'Stopped: ', RESET, $instance, "\n";
	
} elsif ($action eq 'install') {
	
	# installs the instance as a system service (auto load on boot)
	# this depends on your system and it might not work, in which case
	# the program will die with a lovely message
	# ... but currently... this is a TODO
	my $instance = shift;
	die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$instance};
	unless ($instances->{$instance}->server->installed) {
		print YELLOW, 'Server is not installed, installing...', RESET, "\n";
		$instances->{$instance}->server->install;
		print GREEN, 'Server is installed', RESET, "\n";
	}
	$instances->{$instance}->install;

#} elsif ($action eq 'copy-data') {
	#my $src = shift;
	#my $dst = shift;
	
	#die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$src};
	#die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$dst};

	#$instances->{$instance}->copy_data($instances->{$instance}->get_data_folder());

} elsif ($action eq 'status') {
	
	my $instance = shift;
	die 'no instance by that name, use list to see what instances are available' unless defined $instances->{$instance};
	exit not $instances->{$instance}->status;
	
} else {
	esm_error('Invalid action');
}

exit 0;

__END__

=head1 NAME

elastoman-instance - ElasticSearch instance manager

=head1 SYNOPSIS

Show a list of instances and their status (not available, down or up) as well as each instance's server software status.

  elastoman-instance [OPTIONS] list

Setup a new instance or update the configuration of an instance which already exists

  elastoman-instance [OPTIONS] setup <instance name>
  
Start an instance. If the instance was not setup yet, this will run setup too.

  elastoman-instance [OPTIONS] start <instance name>

Stop an instance.

  elastoman-instance [OPTIONS] stop <instance name>

Install an instance as a system service. Currently only supporting Slackware and Debian distributions. The service start scripts are fully compatible with System V.

  elastoman-instance [OPTIONS] install <instance name>

Check instance status. Use this for automating tasks as this command simply returns an error code

  elastoman-instance [OPTIONS] status <instance name>

=head1 OPTIONS

Specify a configuration file other than the default

  --config-file <path to the config file>

=cut
