#!/usr/bin/env perl

use warnings;
use strict;

BEGIN {
	use Cwd qw/abs_path/;
	use File::Basename;
	if (-d dirname(abs_path($0)) . '/../lib') {
		unshift @INC, dirname(abs_path($0)) . '/../lib';
	}
	if (-d dirname(abs_path($0)) . '/../extlib/lib/perl5') {
		unshift @INC, dirname(abs_path($0)) . '/../extlib/lib/perl5';
	}
}

use Data::Dumper;
use File::Basename;
use Getopt::Long;
use Pod::Usage;
use Term::ANSIColor ':constants';

use JSON;

use Elastoman::Index;

######################################################################## process options

my $opts = {
	'config' => './indexes.json',
	'debug' => 0,
	'force' => 0,
	'interactive' => 0,
	'help' => 0,
	'man' => 0
};

GetOptions($opts, 
	'man', 
	'help', 
	'force', 
	'debug!', 
	'interactive!', 
	'source=s', 
	'config=s', 
	'backup-prefix=s', 
	'backup-folder=s'
);

#my $dbg_mode = 0;

print 'debug mode: ', GREEN, $opts->{'debug'}?'on':'off', RESET, "\n";
print 'interactive mode: ', GREEN, $opts->{'interactive'}?'on':'off', RESET, "\n";;

pod2usage({-exitval => 1, -verbose => 1}) if ($opts->{'help'});
pod2usage({-exitval => 1, -verbose => 2}) if ($opts->{'man'});

print "configuration file: ", GREEN, $opts->{'config'}, RESET, "\n";

open(my $fh, '<', $opts->{'config'}) or die RED, 'could not open config file: ', $opts->{'config'}, RESET;
my @l = <$fh>;
close ($fh);
my $json = join '', @l;
my $cfg = decode_json($json);

my $index = shift;

unless (defined $index and defined $cfg->{$index}) {
	# todo: list all indexes
	
	print YELLOW, 'list of indexes available', RESET, "\n";
	foreach my $m (keys %{$cfg}) {
		unless (grep /^$m$/, qw/host port debug suffix prefix mappings-folder/) {
			print GREEN, "\t", $m, RESET, "\n";
		}
	}
	
	exit 1;
}

my $icfg = $cfg->{$index};
foreach (qw/host port suffix prefix mappings-folder debug/) {
	$icfg->{$_} = $cfg->{$_};
}

die 'bad config' unless defined $cfg->{'host'} and defined $cfg->{'port'};
Elastoman::Communicator::server($cfg->{'host'}, $cfg->{'port'});
Elastoman::Communicator::debug($opts->{'debug'});
Elastoman::Communicator::interactive($opts->{'interactive'});

$index = Elastoman::Index->new($icfg);

if (! @ARGV) {
	# todo: list all actions for the index
	my @tasks = $index->tasks();
	
	print RED, 'you need to provide at least one task.', RESET, "\n";
	print YELLOW, 'available tasks: ', RESET, "\n";
	print "\t", GREEN, $_, RESET, "\n" foreach (@tasks);
	
	exit 1;
}

while (@ARGV) {
	my $c = shift;
	
	if ($c eq 'status') {
		my $s = $index->status();
		print YELLOW, 'Installed versions: ', RESET, "\n";
		foreach (@{$s->{versions}}) {
			print "\t", GREEN, $_, RESET, "\n";
		}
		print YELLOW, 'Installed indexes: ', RESET, "\n";
		foreach (keys %{$s->{indexes}}) {
			print "\t", GREEN, $_, RESET, "\n";
		}
	} elsif ($c eq 'install') {
		$index->install($opts->{force});
	} elsif ($c eq 'upgrade') {
		$index->upgrade();
	} elsif ($c eq 'alias') {
		if ($opts->{force}) {
			$index->alias(0, 1);
		} else {
			$index->alias();
		}
	} elsif ($c eq 'clean') {
		$index->clean();
	} elsif ($c eq 'backup') {
		die 'no prefix provided' unless defined $opts->{'backup-prefix'};
		die 'no backup folder provided' unless defined $opts->{'backup-folder'};
		$index->backup($opts->{'backup-folder'}, $opts->{'backup-prefix'});
	} elsif ($c eq 'index-from-backup') {
		die 'no prefix provided' unless defined $opts->{'backup-prefix'};
		die 'no backup folder provided' unless defined $opts->{'backup-folder'};
		$index->index_from_backup($opts->{'backup-folder'}, $opts->{'backup-prefix'});
	} elsif ($c eq 'index-from-index') {
		die 'no source index provided' unless defined $opts->{'source'};
		$index->index_from_index($opts->{'source'});
	} else {
		$index->run($c);
	}
}

__END__

=head1 NAME

elastoman-index - Elastoman's index manager

=head1 SYNOPSIS

  elastoman-index [OPTIONS] <index> <task> [<task> ...]
	
An B<index> must be one of the configured indexes in your configuration. The <task> must be one of:

  install  - install the index with the mapping associated
  update   - increments the version of an index and installs the new index version (only valid for versioned indexes)
  alias    - fixes aliases for an index (only valid for indexes with aliases)
  cleanup  - removes any deprecated indexes/aliases
  backup   - creates backup of an index, requires option 'b' and 'p' to be defined
  index-from-backup - restores a backup into an index. requires option 'b' and 'p' to be defined
  index-from-index  - indexes documents from one index to another. requires option 's' to be defined
  cleanup  - removes any deprecated indexes/aliases
  replicas - sets the number of replicas according to what is specified in the configuration
  
You can also provide extra tasks by adding named commands in the configuration. Please check the documentation for more info.

=head1 OPTIONS

  --config <path to configuration file>
	use a non-default configuration file
  --source <source index>
	the index from where to fetch documents when using the index-from-index task
  --backup-folder <folder>
	the backup folder where to store the backup files
  --backup-prefix <prefix>
	the prefix to use for the backup files
  --interactive
	interactive mode, will ask for user input before sending any command to elasticsearch
  --force
	force action, currently necessary when installing an index and the index is already installed
  --debug
	enable debug mode which will log to a file (if a log entry is provided in the configuration) or to the stdout

=head1 AUTHOR

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.3 or,
at your option, any later version of Perl 5 you may have available.

=cut
