package Elastoman;

our $VERSION = '0.1';

1;

__END__

=head1 NAME

Elastoman - ElasticSearch instance manager

=head1 VERSION

Version 0.1

=head1 DESCRIPTION

Elastoman is a set of tools for managing ElasticSearch instances. It is 
aimed at administrators and developers alike.

=head1 REQUIREMENTS

You need to have Java installed if you wish to start an ElasticSearch
instance.

Currently only Linux is supported.

=head1 CONFIGURATION

If you wish to install a new ElasticSearch server you need to create a
configuration file. Check out an example at L<http://bitbucket.org/vredens/elastoman/raw/master/instances.yml.dist>

To manage indexes you need another configuration file. Check out the
template at L<https://bitbucket.org/vredens/elastoman/raw/master/indexes.json.dist>

=head1 INSTALLATION

  perl Makefile.PL
  make
  make install

=head1 RUNNING

=head2 Instance manipulation

  $ elastoman-instance --help

Will print out more help on how to run it.

=head2 Index manipulation

The _elastoman-index_ command lets you 

  $ elastoman-index --help

=head1 SEE ALSO

ElasticSearch at L<http://www.elasticsearch.org/>
Elastoman wiki at L<https://bitbucket.org/vredens/elastoman/wiki/Home>

=head1 AUTHOR

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.1 or,
at your option, any later version of Perl 5 you may have available.

=cut

