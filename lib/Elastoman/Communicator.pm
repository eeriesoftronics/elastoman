package Elastoman::Communicator;

use Carp;
use Term::ANSIColor ':constants';

#use EerieSoft::RecThis qw/rec_this rec_this_dump/;
use JSON;
use URI;
use URI::Escape;

my $host = 'localhost';
my $port = 9200;

my $interactive = 0;
my $debug = 0;

sub server {
	$host = shift;
	$port = shift;
}

sub debug {
	$i = shift;
	
	$debug = $i if defined $i;
	
	return $debug;
}

sub interactive {
	$i = shift;
	
	$interactive = $i if defined $i;
	
	return $interactive;
}

sub _continue_or_abort {
	print YELLOW, 'Continue? [y|n] ', RESET;
	while(<STDIN>) {
		s/\n|\r//g;
		if ($_ eq 'y') {
			return 1;
		} elsif ($_ eq 'n') {
			exit 1;
		} else {
			print RED, "use either 'y', 'n' or abort the script. Continue [y|n]", RESET;
		}
	}
}

sub request {
	my $method = shift;
	my $uri = shift;
	my $params = shift;
	my $data = shift;

	# convert data to json string if data is a hash ref
	$data = encode_json($data) if (defined $data and ref $data eq 'HASH');
	
	$uri = URI->new("http://$host:$port/$uri");
	
	# build query parameters
	if (defined $params) {
		$uri->query_form($params);
	}

	my $req = "curl -s -X$method '$uri'";
	
	$req .= " -d \'$data\'" if (defined $data);

	#rec_this 10, 'running: ', $req if $debug;
	print 'running: ', GREEN, $req, RESET, "\n" if $interactive;
	_continue_or_abort($req) if ($interactive);

	my $res = `$req`;
	
	croak 'error in elasticsearch response' if ($?);

	eval {
		$res = decode_json($res);
	};
	if ($@) {
		croak 'elasticsearch error: ', $res;
	}
	
	return $res;
}

sub indexes_and_aliases {
	$d = request('GET', '_aliases');

	foreach my $i (keys %$d) {
		my @_as = keys %{$d->{$i}->{'aliases'}};
		
		if (@_as) {
			foreach (@_as) {
				$aliases->{$_} = {} unless defined $aliases->{$_};
				$indexes->{$i} = {} unless defined $indexes->{$i};
				
				$aliases->{$_}->{$i} = encode_json($d->{$i}->{'aliases'}->{$_});
				$indexes->{$i}->{$_} = encode_json($d->{$i}->{'aliases'}->{$_});
			}
		} else {
			$aliases->{'_no-alias'} = {} unless defined $aliases->{'_no-alias'};
			$aliases->{'_no-alias'}->{$i} = '';
			$indexes->{$i} = {} unless defined $indexes->{$i};
		}
	}
	
	return {
		'indexes' => $indexes,
		'aliases' => $aliases
	};
}

sub search {
	my ($index, $type, $query, $params) =  @_;

	my $uri = 

	my $uri = defined $index ? $index . '/' : '_all/';
	$uri .= $type . '/' if defined $type;
	$uri .= '_search';

	return request('GET', $uri, $params, $data);
}

1;

__END__

=head1 NAME

Elastoman Communicator

=head1 SYNOPSIS

=head1 DESCRIPTION

A library of functions for sending requests and receiving responses. At the moment
of this writing this was done using command line curl but in the future it might
use the excelent ElasticSearch.pm and an underline transport layer such as Curl.

=head1 FUNCTIONS

=head2 send (index, type, method, query, params)

Receives an index name, which can be a string such as 'index1,index2'.
The type is the document type, can be '_all' or undef

=head1 AUTHOR

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.3 or,
at your option, any later version of Perl 5 you may have available.

=cut
