package Elastoman::Server;

use 5.010;
use strict;
use warnings;

use Carp;
use Cwd              qw/abs_path getcwd/;
use Data::Dumper;
use File::Path       qw/make_path/;
use File::Basename;
use File::Copy       qw/cp/;
use Term::ANSIColor  ':constants';

use LWP::Simple;

sub new {
	my $class  = shift;
	my $name = shift;
	my $self = shift;
	
	croak 'server requires a location' unless defined $self->{'location'};
	croak 'server requires a version'  unless defined $self->{'version'};
	croak 'server requires a base-uri' unless defined $self->{'base-uri'};
	
	# we need a name
	$self->{'name'} = $name;
	
	# location assumes a new meaning inside a server instance
	$self->{'prefix'}   = $self->{'location'};
	$self->{'location'} = $self->{'prefix'} . '/elasticsearch-' . $self->{'version'};
	
	$self->{'installed'} = 0;
	if (-d $self->{'location'} and -e $self->{'location'} . '/bin/elasticsearch') {
		$self->{'installed'} = 1;
	}
		
	bless $self, $class;
}

sub location {
	my $self = shift;
	
	return $self->{'location'};
}

sub installed {
	my $self = shift;

	# update installed status if the status is not installed
	if (not $self->{'installed'} and -d $self->{'location'} and -e $self->{'location'} . '/bin/elasticsearch') {
		$self->{'installed'} = 1;
	}
	
	return $self->{'installed'};
}

sub install {
	my $self = shift;
	
	my $es_file  = 'elasticsearch-' . $self->{'version'} . '.tar.gz';
	my $es_dluri = $self->{'base-uri'} . $es_file;

	my $bwd = getcwd;
	
	# make the prefix folder if it doesn't exist yet
	make_path $self->{'prefix'} unless -d $self->{'prefix'};
	
	# location must now be converted to absolute
	$self->{'location'} = abs_path($self->{'location'});
	
	# place ourselves on the prefix folder
	chdir $self->{'prefix'};

	# download elasticsearch
	unless (-f $es_file or -d $self->{'location'}) {
		print GREEN, 'downloading elasticsearch', RESET, "\n";
		getstore($es_dluri, $es_file);
		croak "Error fetching $es_file from $es_dluri" unless -f $es_file;
	}

	# untar the elasticsearch .tar.gz
	unless (-d $self->{'location'}) {
		print GREEN, 'extracting elasticsearch', RESET, "\n";
		my $tar = Archive::Tar->new;
		$tar->read($es_file);
		$tar->extract();
		die "Could not extract $es_file, no ", abs_path($self->{'location'}), " found" unless -d abs_path($self->{'location'});
	}

	# install plugins
	if (defined $self->{'plugins'}) {
		my $plugins = $self->{'plugins'};
		chdir $self->{'location'};
		foreach (keys %{$plugins}) {
			print GREEN, 'plugin already installed: ', $_, RESET, "\n" and next if -d "plugins/$_";
			print GREEN, 'installing plugin: ', $_, RESET, "\n";
			`./bin/plugin -install "$plugins->{$_}"`;
			croak 'could not install plugin ', $_ unless -d "plugins/$_";
		}
	}
	
	# TODO: for each folder in the plugins folder check if the plugin is marked for installation, if not, remove it

	chdir $bwd;
	
}

sub uninstall {
	my $self = shift;
	
	# todo: remove the server
}

1;

__END__

=head1 NAME

Elastoman::Server - an Elastoman server

=head1 SYNOPSIS

TODO

=head1 DESCRIPTION

Represents the definition of an elasticsearch server. A server is simply
the software which can launch elasticsearch instances.

=head1 METHODS

=head2 new

Creates a new server. This will not install anything, it simply creates
a server configuration which you can then install, remove or just check
if it is installed.

=head2 installed

returns true or false if the server is installed or not.

=head2 install

Downloads the server into the location provided. Note that the location
is where the server folder will be located, it is not the actual server
location. So if you specify '/opt/elastic-server/' in the location then
the server will be installed in '/opt/elastic-server/elasticsearch-<version>'

=head2 uninstall

Removes the server installation.

=head2 location

Returns the server location.

=head1 AUTHOR

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.3 or,
at your option, any later version of Perl 5 you may have available.

=cut
