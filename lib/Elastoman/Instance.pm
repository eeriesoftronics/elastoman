package Elastoman::Instance;

use 5.010;
use strict;
use warnings;

use Carp;
use Cwd              qw/abs_path getcwd/;
use File::Path       qw/make_path/;
use File::Basename;
use File::Copy       qw/cp/;
use Term::ANSIColor  ':constants';

use Elastoman::Server;

sub get_pid_from_file {
	my $f = shift;

	return 0 unless -r $f;

	open (my $pidfh, '<', $f);
	my $pid = <$pidfh>;
	close ($pidfh);

	return $pid;
}

sub _valid_server {
	my $server = shift;
	my $validate_paths = shift;
	
	foreach (qw/location base-uri version/) {
		return 0 unless defined $server->{$_};
	}
	
	if ($validate_paths) {
		return 0 unless -e $server->location . '/elasticsearch-' . $server->{'version'} . '/bin/elasticsearch';
	}
	
	return 1;
}

sub new {
	my $class  = shift;
	my $name   = shift || croak 'name is not defined';
	my $self   = shift;
	
	croak 'no server provided' unless defined $self->{'server'};
	croak 'invalid server configuration' unless $self->{'server'}->isa('Elastoman::Server');
	
	$self->{'name'}     = $name;
	$self->{'location'} = $self->{'location'};
	$self->{'pid-file'} = $self->{'location'} . '/pid-file';
	$self->{'scripts-folder'}  = $self->{'location'} . '/bin';

	# always replace the config folder, ignore any settings in config-params
	$self->{'config-params'}->{'path.conf'} = 'config';
	$self->{'config-params'}->{'path.data'} = 'data' unless defined $self->{'config-params'}->{'path.data'};
	$self->{'config-params'}->{'path.logs'} = 'logs' unless defined $self->{'config-params'}->{'path.logs'};
	
	bless $self, $class;
}

sub location {
	my $self = shift;
	
	abs_path($self->{'location'});
}

sub pidfile {
	my $self = shift;
	
	abs_path($self->{'pid-file'});
}

sub installed {
	my $self = shift;
	
	return 0 unless defined $self->location and -d $self->location;
	
	return 1;
}

sub server {
	my $self = shift;
	
	return $self->{'server'};
}

sub deploy {
	my $self   = shift;
	
	$self->{'server'}->installed or croak 'server not installed';
	
	# create folder structure
	unless (-d $self->{'location'}) {
		make_path ($self->{'location'}) or croak 'could not make path';
	}

	# change to the instance location due to potential relative paths in 
	# configurations
	# also, reset paths to absolute path so the configurations are correct
	my $bwd = getcwd;
	chdir $self->{'location'};
	$self->{'config-params'}->{'path.conf'} = abs_path($self->{'config-params'}->{'path.conf'});
	$self->{'config-params'}->{'path.data'} = abs_path($self->{'config-params'}->{'path.data'});
	$self->{'config-params'}->{'path.logs'} = abs_path($self->{'config-params'}->{'path.logs'});	
	chdir $bwd;

	make_path($self->{'scripts-folder'}) or croak 'could not create scripts folder' unless -d $self->{'scripts-folder'};
	make_path($self->{'config-params'}->{'path.conf'}) or croak 'could not create config folder' unless -d $self->{'config-params'}->{'path.conf'};
	make_path($self->{'config-params'}->{'path.data'}) or croak 'could not create data folder' unless -d $self->{'config-params'}->{'path.data'};
	make_path($self->{'config-params'}->{'path.logs'}) or croak 'could not create logs folder' unless -d $self->{'config-params'}->{'path.logs'};

	# copy and update the config
	my $cfg_file = $self->{'config-params'}->{'path.conf'} . '/elasticsearch.yml';
	unless (-f $cfg_file) {
		open (my $ofh, '>', $cfg_file) or die 'could not open destination config file';
		foreach (keys %{$self->{'config-params'}}) {
			print $ofh $_, ': ', $self->{'config-params'}->{$_}, "\n";
		}
		close ($ofh);
	}
	my $_src = $self->{'server'}->location . '/config/logging.yml';
	my $_dst = $self->{'config-params'}->{'path.conf'} . '/logging.yml';
	cp($_src, $_dst) or croak 'could not copy logging configurations' unless (-f $_dst);
	
	$self->_script;
}

sub status {
	my $self = shift;

	# if not installed, return -1
	return -1 unless $self->installed;

	# check PID status, returns 0 or 1 (down or up)
	my $pid = get_pid_from_file($self->pidfile);
	$pid and kill 0, $pid;
}

sub start {
	my $self = shift;
	
	# validate that this instance is installed properly
	croak 'instance is not installed' unless $self->installed;

	(system ($self->_gen_script_filename, 'start') == 0) or croak 'could not start instance';
}

sub stop {
	my $self = shift;

	# validate that this instance is installed properly
	croak 'instance is not installed' unless $self->installed;
	croak 'instance is not started' unless $self->status == 1;

	(system($self->_gen_script_filename, 'stop') == 0) or croak 'could not stop instance';
	
}

sub install {
	my $self = shift;
	
	croak 'you need root privileges to install an instance in the system' unless $> == 0;
	
	# TODO: deploy the script into one of the many system v types
	# Debian: DONE
	# RHEL
	# Slackware: DONE
	# all others are fail
	
	my $script = $self->_gen_script_filename;
	my $script_fn = 'elasticsearch-' . $self->{'name'};
	
	if (-f '/etc/slackware-version') { ################################## slackware
		cp($script, '/etc/rc.d/rc.' . $script_fn) or die 'failed to copy script file';
		for (3..5) {
			symlink '/etc/rc.d/rc.' . $script_fn, '/etc/rc.d/rc' . $_ . '.d/S99' . $script_fn or die 'could not create symlink';
		}
		for ((1,6)) {
			symlink '/etc/rc.d/rc.' . $script_fn, '/etc/rc.d/rc' . $_ . '.d/K99' . $script_fn or die 'could not create symlink';
		}
	} elsif (-f '/etc/debian_version') { ################################ debian
		cp($script, '/etc/init.d/' . $script_fn) or die 'failed to copy script file';
		for (2..5) {
			symlink '/etc/init.d/' . $script_fn, '/etc/rc' . $_ . '.d/S99' . $script_fn or die 'could not create symlink';
		}
		for ((1,6)) {
			symlink '/etc/init.d/' . $script_fn, '/etc/rc' . $_ . '.d/K99' . $script_fn or die 'could not create symlink';
		}
	} elsif (-f '/etc/rhel-version') { ################################## RHEL
		...
	} else {
		croak 'your OS/Disto is not supported';
	}

}

sub _gen_script_filename {
	my $self = shift;

	return $self->{'scripts-folder'} . '/' . $self->{'name'} . '-launcher.sh';
}

sub _script {
	my $self = shift;
	
	my $sf = $self->_gen_script_filename;
	open (my $ifh, '<', abs_path(__FILE__) . '/elasticsearch-instance.sh.template') or die "could not open the template";
	open (my $ofh, '>', $sf) or die "could not open the script: $sf";
	my $_b = 0;
	while (<$ifh>) {
		if (/^#ELASTOMAN-START/) {
			$_b = 1;
			print $ofh $_;
		} elsif (/^#ELASTOMAN-STOP/) {
			$_b = 0;
			print $ofh $_;
		} elsif ($_b and /(\w+)=/) {
			my $var = $1;
			if ($var eq 'INSTANCE_NAME') {
				print $ofh $var, '=', $self->{'name'}, "\n";
			} elsif ($var eq 'MAX_OPEN_FILES') {
				if (defined $self->{'os-configs'}->{'max-open-files'}) {
					print $ofh $var, '=', $self->{'os-configs'}->{'max-open-files'}, "\n";
				}
			} elsif ($var eq 'AS_USER') {
				if (defined $self->{'os-configs'}->{'user'}) {
					print $ofh $var, '=', $self->{'os-configs'}->{'user'}, "\n";
				}
			} elsif ($var eq 'PID_FILE') {
				print $ofh $var, '=', $self->pidfile, "\n";
			} elsif ($var eq 'ES_SERVER_LOCATION') {
				print $ofh $var, '=', abs_path($self->{'server'}->location), "\n";
			} elsif ($var eq 'ES_PATH_CONF') {
				print $ofh $var, '=', $self->{'config-params'}->{'path.conf'}, "\n";
			} elsif ($var eq 'JAVA_HOME') {
				print $ofh $var, '=', $self->{'java'}->{'home'}, "\n";
			} elsif ($var eq 'ES_HEAP_SIZE') {
				print $ofh $var, '=', $self->{'memory'}, "\n";
			} elsif ($var eq 'ES_JAVA_OPTS') {
				print $ofh $var, '=', $self->{'java'}->{'opts'}, "\n";
			} else {
				print $ofh $_;
			}
		} else {
			print $ofh $_;
		}
	}
	close ($ifh);
	close ($ofh);
	
	chmod 0755, $sf;
}

1;

__END__

=head1 NAME

Elastoman::Instance - an Elastoman instance

=head1 SYNOPSIS

=head1 DESCRIPTION

Represents an ElasticSearch Server instance.

=head1 METHODS

=head2 new ($name, $config)

Creates a new Elastoman::Instance. The I<name> must be a string which
represents the instance name. The configuration hash needs to have at 
least these keys:

=over 1
=item server - a Elastoman::Server instance
=item location - a valid location to install the server, this is the 
root folder, the actual instance will be located in 
I<location>/I<instance_name>
=back

=head2 installed

Returns true or false if the instance is installed or not.

=head2 status

Returns current instance status code.

=over 1
=item -1 means that the instance is not installed
=item 0 not started
=item 1 instance is running
=back

=head2 start

Starts the instance. Croaks if the instance is already running. Returns 
true if it managed to start the instance, false otherwise.

Might require root privileges.

=head2 stop

Stops the instance. Returns true if it managed to stop it. Croaks if the 
instance is not started or if any error ocurred during the atempt to
shutdown.

Might require root privileges.

=head2 install

Requires root privileges.

Tries to install a startup script on your system. This functionality is
very limited. If you rather just want a SystemV ready script to be 
generated then use the B<script>

=head1 AUTHOR

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.3 or,
at your option, any later version of Perl 5 you may have available.

=cut
