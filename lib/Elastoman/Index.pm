package Elastoman::Index;

use Carp;
use Data::Dumper;
use List::Util 'max';
use POSIX;

#use EerieSoft::RecThis qw/rec_this rec_this_dump/;
use JSON;

use Elastoman::Communicator;

sub new {
	my $class = shift;
	my $_self = shift;

	# base configuration
	my $self = {
		'base-name' => $_self->{'base-name'},
		'mapping'   => $_self->{'mappings-folder'} . '/' . $_self->{'mapping'},
		'suffix'    => $_self->{'suffix'},
		'enable-suffix' => (defined $_self->{'enable-suffix'} and $_self->{'enable-suffix'} eq 'true'),
		'prefix'    => $_self->{'prefix'},
		'enable-prefix' => (defined $_self->{'enable-prefix'} and $_self->{'enable-prefix'} eq 'true'),
		'versioned' => (defined $_self->{'versioned'} and $_self->{'versioned'} eq 'true'),
		'aliases'   => undef,
		'commands'  => undef,
		# 
		'synced' => 0
	};
	
	# validate configurations
	croak 'invalid base name' if ($_self->{'base-name'} !~ /^[-_\w]+$/);
	croak 'could not find mappings file [', $m, ']' unless -f $self->{'mapping'};
	
	push @{$self->{'tasks'}}, qw/clean install backup index-from-backup index-from-index/;
	push @{$self->{'tasks'}}, qw/upgrade/ if ($self->{'versioned'});
	
	if (defined $_self->{'aliases'}) {
		push @{$self->{'tasks'}}, qw/alias/;
		$self->{'aliases'} = $_self->{'aliases'};
	}
	
	if (defined $_self->{'commands'}) {
		push @{$self->{'tasks'}}, keys %{$_self->{'commands'}};
		$self->{'commands'} = $_self->{'commands'};
	}

	$self->{'index-mask'} = $self->{'base-name'};
	$self->{'index-mask'} = $self->{'prefix'} . '_' . $self->{'index-mask'} if ($self->{'enable-prefix'});
	$self->{'index-mask'} = $self->{'index-mask'} . '_v(\d+)' if ($self->{'versioned'});
	$self->{'index-mask'} = $self->{'index-mask'} . '_' . $self->{'suffix'} if ($self->{'enable-suffix'});
	#rec_this 10, 'index mask ', $self->{'index-mask'};
	
	bless $self, $class;
}

sub _fetch_status {
	my $self = shift;
	
	return if $self->{'synced'};
	
	$d = Elastoman::Communicator->indexes_and_aliases();
	
	foreach my $i (keys %{$d->{'indexes'}}) {
		if ($i =~ /$self->{'index-mask'}/) {
			push @{$self->{'versions'}}, $1 if ($self->{'versioned'});
			$self->{'indexes'}->{$i} = $d->{'indexes'}->{$i};
		}
	}
	
	$self->{installed} = 0;
	$self->{installed} = 1 if ($self->{indexes});
	
	#rec_this 5, 'fetched status';

	if ($self->{versioned}) {
		#rec_this 5, 'versioned index';
		if (@{$self->{versions}}) {
			#rec_this 10, 'calculating max version';
			$self->{version} = max(@{$self->{versions}});
		} else {
			#rec_this 10, 'no version found, assumimng 0';
			$self->{version} = 0;
		}
	}

	$self->{'synced'} = 1;
}

sub _gen_name {
	my $base = shift or croak 'invalid base name';
	my $prefix = shift;
	my $suffix = shift;
	my $version = shift;
	
	my $name = $base;
	$name = $prefix . '_' . $name   if ($prefix);
	$name = $name . '_v' . $version if ($version);
	$name = $name . '_' . $suffix   if ($suffix);

	return $name;
}

sub _index_name {
	my $self = shift;
	my $version = shift;
	
	croak 'no version provided' if $self->{versioned} and not defined $version;
	
	my $name = $self->{'base-name'};
	$name = $self->{'prefix'} . '_' . $name if ($self->{'enable-prefix'});
	$name = $name . '_v' . $version         if ($self->{'versioned'});
	$name = $name . '_' . $self->{'suffix'} if ($self->{'enable-suffix'});

	return $name;
}

sub _get_mapping_from_file {
	my $file = shift or croak 'no filename provided';
	
	croak 'invalid filename ', $file unless -f $file;
	
	open(my $fh, '<', $file);
	my $t = $/;
	$/ = '';
	my $text = <$fh>;
	close($fh);
	$/ = $t;
	
	return $text;
}

sub _get_bulk_from_file {
	my $file = shift or croak 'no filename provided';
	
	croak 'invalid filename ', $file unless -f $file;
	
	open(my $fh, '<', $file);
	my @text = <$fh>;
	close($fh);
	
	return @text;
}

sub _start_scan {
	my $in = shift;
	my $size = shift;
	my $timeout = shift;
	
	$size = 1000 unless defined $size;
	$timeout = '5m' unless defined $timeout;
	
	my $search = '{"query":{"match_all":{}}}';
	my $options = {
		'scroll' => $timeout,
		'size'   => $size,
		'search_type' => 'scan'
	};
	
	my $r = Elastoman::Communicator::request('GET', $in . '/_search', $options, $search);
	
	my $scroll_id = $r->{'_scroll_id'};
	my $total = $r->{'hits'}->{'total'};
	
	delete $options->{'search_type'};
	$options->{'scroll_id'} = $scroll_id;
	
	return {
		options => $options,
		total   => $total
	};
}

sub _continue_scan {
	my $scan = shift;

	return undef if $scan->{total} == 0;
	
	my $r = Elastoman::Communicator::request('GET', '_search/scroll', $scan->{options}, undef);
	
	return undef unless $r->{'_shards'}->{'failed'} == 0;
	
	$scan->{total} -= @{$r->{'hits'}->{'hits'}};
	$scan->{last_call_took} = $r->{took};

	$r->{'hits'}->{'hits'};
}

sub status {
	my $self = shift;
	
	$self->_fetch_status;
	
	my $r = {
		'installed' => ($self->{'indexes'}) ? 1 : 0,
		'versions'  => ($self->{'versions'}) ? \@{$self->{'versions'}} : [],
		'indexes'   => ($self->{'indexes'}) ? \%{$self->{'indexes'}} : {},
	};

	return $r;
}

sub tasks {
	my $self = shift;
	
	return @{$self->{'tasks'}};
}

sub install {
	my $self = shift;
	my $force = shift;
	
	$self->_fetch_status;
	
	# generate version, if index is versioned
	$self->{version} = 1 if ($self->{versioned} and $self->{version} == 0);

	# generate index name
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);
	
	# check if index is already installed or not. if it is installed then $force needs to be true and index will be deleted
	if ($self->{installed}) {
		croak 'index already installed' unless $force;
		
		Elastoman::Communicator::request('DELETE', $index_name);
	}
	
	# get the mapping
	my $text = _get_mapping_from_file($self->{'mapping'});
	
	# debug
	#rec_this 5, 'index name: ', $index_name;
	#rec_this 5, 'mapping: ', $self->{'mapping'};
	#rec_this 5, $text;
	
	# install index
	my $res = Elastoman::Communicator::request('POST', $index_name, undef, $text);
	unless ($res->{'acknowledged'} == 1) {
		if (defined $res->{error}) {
			croak 'failed to create the index: ' . $res->{error};
		}
		croak 'failed to create the index';
	}

	$self->{'synced'} = 0
}

sub upgrade {
	my $self = shift;
	
	$self->_fetch_status;
	
	# generate version, if index is versioned
	$self->{version}++ if $self->{versioned};

	# generate index name
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);
	
	# get the mapping
	my $text = _get_mapping_from_file($self->{'mapping'});

	# debug
	#rec_this 5, 'index name: ', $index_name;
	#rec_this 5, 'mapping: ', $self->{'mapping'};
	#rec_this 5, $text;
	
	# install index
	croak 'failed to upgrade index' unless Elastoman::Communicator::request('POST', $index_name, undef, $text)->{'acknowledged'} == 1;

	$self->{'synced'} = 0
}

sub clean {
	my $self = shift;
	
	$self->_fetch_status;
	
	# generate index name
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);

	foreach (keys %{$self->{indexes}}) {
		my @_aliases = keys %{$self->{indexes}->{$_}};
		#rec_this 10, $_, ' has aliases: ', join(', ', @_aliases);
		if ($_ ne $index_name and @_aliases == 0) {
			#rec_this 5, 'removing index: ', $_;
			croak 'could not delete index ', $_ unless Elastoman::Communicator::request('DELETE', $_)->{ok} == 1;
		}
	}
	
	$self->{synced} = 0;
	
	$self->alias;
}

sub alias {
	my $self = shift;
	my $cleanup = shift;
	my $reinstall = shift;
	
	unless ($self->{aliases}) {
		#rec_this 5, 'requested alias update on an index which does not support aliases';
		return;
	}
	
	$self->_fetch_status;
	
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);
	
	my %index_aliases = map {
			my $an = _gen_name($_->{name}, $_->{prefix} == 1 ? $self->{prefix} : undef, $_->{suffix} == 1 ? $self->{suffix} : undef);
			($an, $_)
		} @{$self->{aliases}};

	my @actions;
	foreach my $i (keys %{$self->{indexes}}) {
		if ($i ne $index_name) {
			foreach my $a (keys %{$self->{indexes}->{$i}}) {
				if ($cleanup or grep { $a eq $_ } keys %index_aliases) {
					#rec_this 10, 'removing other index ', $i, ' from alias ', $a;
					push @actions, { "remove" => { "index" => $i, "alias" => $a } }
				}
			}
		} else {
			my @aliases = keys %{$self->{indexes}->{$i}};
			foreach my $a (@aliases) {
				unless (!$reinstall and grep { $a eq $_ } keys %index_aliases) {
					#rec_this 10, 'removing index ', $i, ' from alias ', $a;
					push @actions, { "remove" => { "index" => $i, "alias" => $a } }
				}
			}
			foreach my $an (keys %index_aliases) {
				unless (!$reinstall and grep { $_ eq $an } @aliases) {
					#rec_this 10, 'adding index ', $i, ' to alias ', $an;
					if (defined $index_aliases{$an}->{filter}) {
						push @actions, { "add" => { "index" => $index_name, "alias" => $an, 'filter' => $index_aliases{$an}->{filter} } }
					} else {
						push @actions, { "add" => { "index" => $index_name, "alias" => $an } }
					}
				}
			}
		}
	}
	
	if (@actions) {
		my $data = {
			'actions' => \@actions
		};
		
		croak 'could not update aliases' unless Elastoman::Communicator::request('POST', '_aliases', undef, $data)->{'acknowledged'} == 1;
		
		$self->{'synced'} = 0;
	}
}

sub backup {
	my $self = shift;
	my $folder = shift or croak 'you need to provide a valid folder to place the document backups';
	my $prefix = shift or croak 'you need to provide a prefix for each bulk file';
	
	croak 'invalid folder ', $folder unless -d $folder;
	
	$self->_fetch_status;
	
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);
	my $search = '{"query":{"match_all":{}}}';
	
	my $options = {
		'scroll' => '5m',
		'size'   => 20,
		'search_type' => 'scan'
	};
	
	my $r = Elastoman::Communicator::request('GET', $index_name . '/_search', $options, $search);
	
	my $scroll_id = $r->{'_scroll_id'};
	my $total = $r->{'hits'}->{'total'};
	
	#rec_this 20, '[total:', $total, '][scroll_id:', $scroll_id, ']';

	my $zeros = 1 + ceil(log($total / $options->{size}) / log(10));
	my $iter = 1;
	
	my $fn;
	my @docs;

	delete $options->{'search_type'};
	$options->{'scroll_id'} = $scroll_id;

	# scan through the results
	while($total > 0) {
		$r = Elastoman::Communicator::request('GET', '_search/scroll', $options, undef);

		@docs = @{$r->{'hits'}->{'hits'}};
		$total = $total - @docs;

		$fn = sprintf "$folder/${prefix}_%0${zeros}d.json", $iter;

		open($fh, '>', $fn);
		foreach (@docs) {
			my $header = {
				index => {
					_index => $_->{_index},
					_type  => $_->{_type},
					_id    => $_->{_id}
				}
			};
			
			print $fh encode_json($header), "\n";
			print $fh encode_json($_->{_source}), "\n";
		}
		close($fh);

		#rec_this 20, 'backup iteration: ', $iter, '. backed up ', int @docs, ' documents';

		$iter++;
	}
}

sub index_from_backup {
	my $self = shift;
	my $folder = shift or croak 'you need to provide a valid folder to place the document backups';
	my $prefix = shift or croak 'you need to provide a prefix for each bulk file';
	
	croak 'invalid folder ', $folder unless -d $folder;
	
	$self->_fetch_status;
	
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);

	opendir(my $dh, $folder) or croak 'could not fetch contents from directory ', $folder;
	while (readdir $dh) {
		if (/^${prefix}_/) {
			#rec_this 20, 'bulk indexing from ', "$folder/$_";
			my @bulk = _get_bulk_from_file("$folder/$_");
			
			my $i = 0;
			my $bulk = '';
			for (@bulk) {
				if ($i % 2 == 0) { # header
					my $h = decode_json($_);
					$h->{index}->{_index} = $index_name;
					$bulk .= encode_json($h) . "\n";
				} else {      # body
					$bulk .= $_;
				}
				$i++;
			}
			
			#rec_this_dump 20, $bulk;
			my $_debug = Elastoman::Communicator::debug();
			Elastoman::Communicator::debug(0);
			my $r = Elastoman::Communicator::request('POST', '_bulk', undef, $bulk);
			Elastoman::Communicator::debug($debug);
			
			for (@{$r->{items}}) {
				#rec_this 5, 'failed to index ', $_->{index}->{_id} unless $_->{index}->{ok} == 1;
			}
		}
	}
	closedir($dh);
}

sub index_from_index {
	my $self = shift;
	my $src  = shift;
	
	croak 'no source index provided' unless defined $src;
	
	my $r = Elastoman::Communicator::request('GET', $src . '/_search', {'size' => 0});
	
	croak 'index [', $src, '] does not exist' if defined $r->{status} and $r->{status} == 404;
	croak 'index [', $src, '] is not fully recovered' if $r->{_shards}->{failed} != 0;
	
	# fetch current index name
	$self->_fetch_status;
	my $index_name = $self->_index_name($self->{versioned} ? $self->{version} : undef);
	
	# start a scan search (the size is multiplied by the shards)
	my $scan = _start_scan($src, 100);
	
	# start scan and reindex stage
	my ($h, $b);
	do {
		# fetch more documents from the scan
		$docs = _continue_scan($scan);
		
		if ($docs) {
			# prepare bulk request
			my $text = '';
			my $cnt = 0;
			for (@{$docs}) {
				$h = { index => { _index => $index_name, _type => $_->{_type}, _id => $_->{_id} } };
				$b = $_->{_source};
				$text .= encode_json($h) . "\n";
				$text .= encode_json($b) . "\n";
				$cnt++;
			}

			# send bulk request
			my $_debug = Elastoman::Communicator::debug();
			Elastoman::Communicator::debug(0);
			$r = Elastoman::Communicator::request('POST', '_bulk', undef, $text);
			Elastoman::Communicator::debug($debug);
			
			#rec_this 20, 'bulk indexed ', $cnt, ' documents from index ', $src, ' into index ', $index_name, ' in ', $r->{took} + $scan->{last_call_took}, ' ms';

			# log any failed indexations
			for (@{$r->{items}}) {
				#rec_this 5, 'failed to index ', $_->{index}->{_id} unless $_->{index}->{ok} == 1;
			}
		}
	} while ($docs);
}

sub run {
	my $self = shift;
	my $task = shift;
	
	croak 'that task is not supported by this index' unless defined $self->{commands}->{$task};
	
	my $cmd = $self->{commands}->{$task};
	
	#rec_this 10, 'running: ', $cmd;
	my $a = `$cmd`;
	
	if ($?) {
		#rec_this 5, 'task [', $task, '] error. output start.', "\n", $a;
		#rec_this 5, 'task [', $task, '] error. output end.';
		croak 'error running the task ', $task, ': ', $?;
	} else {
		#rec_this 20, 'task [', $task, '] success. output start.', "\n", $a;
		#rec_this 20, 'task [', $task, '] success. output end.';
	}
}

1;

__END__

=head1 NAME

Elastoman Index

=head1 SYNOPSIS

=head1 FUNCTIONS

=head2 install

=head2 upgrade

=head2 alias ($clean)

Fixes aliases for the index provided.

If $clean is set an true then all other index versions and aliases, which are not configured aliases, are all removed.

=head2 clean

Removes old versions which no longer have aliases associated.

=head2 run

=head1 AUTHOR

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.3 or,
at your option, any later version of Perl 5 you may have available.

=cut
